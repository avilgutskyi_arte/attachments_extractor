<?php


namespace Extractor\Downloader;


class ImapDownloader extends EmailDownloader
{
    public $protocol = "imap";
    public $port     = "993";
    public $flags    = "/imap/ssl";

}