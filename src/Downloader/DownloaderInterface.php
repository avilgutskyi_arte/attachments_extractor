<?php


namespace Extractor\Downloader;


interface DownloaderInterface
{
    public function download();
}
