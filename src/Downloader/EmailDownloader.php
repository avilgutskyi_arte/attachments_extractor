<?php


namespace Extractor\Downloader;


use Extractor\AttributesSetterTrait;
use Extractor\Entities\File;
use Extractor\ExtractorException;
use PhpImap\Exceptions\InvalidParameterException;
use PhpImap\Mailbox;

class EmailDownloader implements DownloaderInterface
{
    use AttributesSetterTrait;

    public $protocol;
    public $host;
    public $port;
    public $flags;
    public $folder = "INBOX";
    public $username;
    public $password;

    public $client;
    public $tmp_folder = "/tmp";

    /**
     * EmailDownloader constructor.
     *
     * @param $attributes array to configure downloader
     * @throws ExtractorException
     */
    public function __construct($attributes)
    {

        if (!is_dir($this->tmp_folder))
            mkdir($this->tmp_folder, 0777, true);

        $this->setAttributes($attributes);

        try {
            $this->client = new Mailbox(
                "{{$this->host}:{$this->port}{$this->flags}}{$this->folder}",
                $this->username,
                $this->password,
                $this->tmp_folder,
                'UTF-8'
            );
        } catch (InvalidParameterException $e) {
            throw new ExtractorException($e->getMessage());
        }

    }

    public function download()
    {
        $files = [];
        try {
            $mailsIds = $this->client->searchMailbox('UNSEEN');
            if (count($mailsIds)) {
                foreach ($mailsIds as $mailsId) {
                    $mail = $this->client->getMail($mailsId);
                    if ($mail->hasAttachments()) {
                        foreach ($mail->getAttachments() as $attachment) {
                            $file            = new File();
                            $file->id        = $attachment->id;
                            $file->name      = $attachment->name;
                            $file->file_path = $attachment->FilePath;
                            $file->setSize();
                            $files[] = $file;
                        }
                    }

                }
            }
        } catch (\Exception $e) {
            throw new ExtractorException($e->getMessage());
        }

        return $files;
    }
}