<?php


namespace Extractor\Downloader;


class PopDownloader extends EmailDownloader
{
    public $protocol = "pop";
    public $port     = "995";
    public $flags    = "/pop3/ssl";

}