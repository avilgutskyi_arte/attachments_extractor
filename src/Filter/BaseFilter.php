<?php


namespace Extractor\Filter;


use Extractor\AttributesSetterTrait;

abstract class BaseFilter implements FilterInterface
{
    use AttributesSetterTrait;
}