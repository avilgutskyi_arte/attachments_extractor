<?php


namespace Extractor\Filter;


use Extractor\Entities\File;

interface FilterInterface
{
    public function filtrate(File $file);
}
