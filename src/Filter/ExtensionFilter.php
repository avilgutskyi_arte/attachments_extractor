<?php


namespace Extractor\Filter;


use Extractor\Entities\File;

class ExtensionFilter extends BaseFilter
{
    /**
     * @var $allowed array with allowed extensions
     */
    public $allowed;

    /**
     * @param File $file to accept or decline, base on filtration rules
     * @return bool
     */
    public function filtrate(File $file)
    {
        $extension = pathinfo($file->name)['extension'];
        if (!empty($extension))
            return in_array($extension, $this->allowed);

        return false;
    }
}