<?php


namespace Extractor;


class ExtractorException extends \Exception
{
    /**
     * ExtractorException constructor.
     *
     * @param string $message  The Exception message to throw.
     * @param int    $code     The Exception code.
     * @param null   $previous The previous throwable used for the exception chaining
     */
    public function __construct($message = "", $code = 0, $previous = null)
    {
        self::writeLog($message);
        parent::__construct($message = "", $code = 0, $previous = null);
    }

    /**
     * @param $log_msg string contains the message to be logged
     */
    public static function writeLog($log_msg)
    {
        $log_filename = "log";
        if (!file_exists($log_filename)) {
            mkdir($log_filename, 0777, true);
        }
        $log_file_data = $log_filename . '/log_' . date('d-M-Y') . '.log';
        file_put_contents($log_file_data, "[" . date('Y-m-d H:i:s') . "] " . $log_msg . "\n", FILE_APPEND);
    }

}