<?php


namespace Extractor;


class Worker
{
    public $extractors;

    /**
     * Worker constructor.
     *
     * @param $configs string json format with configuration data for all extractors
     * @throws ExtractorException
     */
    public function __construct($configs)
    {
        $configs = json_decode($configs, true);
        if (!$configs)
            throw new ExtractorException("Unable to configure extractors. Please check configuration file");
        try {
            foreach ($configs as $config) {
                $this->extractors[] = new Extractor($config);
            }
        } catch (\Exception $e) {
            throw new ExtractorException($e->getMessage());
        }
    }

    /**
     * Starts worker to execute all the extractors
     */
    public function start()
    {
        if ($this->extractors)
            foreach ($this->extractors as $extractor) {
                $extractor->execute();
            }
    }
}