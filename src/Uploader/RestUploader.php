<?php


namespace Extractor\Uploader;


use Extractor\AttributesSetterTrait;
use GuzzleHttp\Client;

abstract class RestUploader implements UploaderInterface
{
    use AttributesSetterTrait;

    /**
     * @var \FileCache         $cache  to be used for caching authentication information
     * @var \GuzzleHttp\Client $client to be used for sending http requests
     */
    public $cache;
    public $client;

    /**
     * RestUploader constructor.
     *
     * @param $attributes array to configure uploader
     */
    public function __construct($attributes)
    {
        $this->cache  = new \FileCache();
        $this->client = new Client(['base_uri' => $this->base_uri]);

        $this->setAttributes($attributes);
    }

}