<?php


namespace Extractor\Uploader;


use Extractor\Entities\File;

interface UploaderInterface
{
    public function upload(File $file);
}
