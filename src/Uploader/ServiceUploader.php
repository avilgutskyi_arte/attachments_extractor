<?php


namespace Extractor\Uploader;


use Extractor\Entities\File;
use Extractor\ExtractorException;

class ServiceUploader extends RestUploader
{
    public $email;
    public $password;
    public $base_uri = 'https://example.com/';

    /**
     * @param File $file to be uploaded
     * @return bool which indicates whether file was uploaded successfully
     * @throws ExtractorException
     */
    public function upload(File $file)
    {
        try {
            $response = $this->client->post('/api/upload', [
                'headers' => [
                    'Authorization' => 'Bearer ' . $this->getToken()
                ],
                'json'    => [
                    'file' => $file->base64_string
                ]
            ]);

            return $response->getStatusCode() == 200;
        } catch (\Exception $e) {
            ExtractorException::writeLog($e->getMessage());
        }

        return false;
    }

    /**
     * @return string with authentication token
     * @throws ExtractorException
     */
    public function getToken()
    {
        $auth = $this->cache->get("ServiceUploaderAuth");
        try {
            if (!$auth) {
                $response = $this->client->post('/gettoken', [
                    'auth' => [$this->email, $this->password]
                ]);

                $auth = json_decode($response->getBody(), true);

                $this->cache->save("ServiceUploaderAuth", $auth, $auth['expires_in']);

            }
        } catch (\Exception $e) {
            throw new ExtractorException($e->getMessage());
        }

        return $auth['access_token'];
    }

}