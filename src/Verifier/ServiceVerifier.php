<?php


namespace Extractor\Verifier;


use Extractor\Entities\File;
use Extractor\ExtractorException;

class ServiceVerifier extends RestVerifier
{
    public $email;
    public $password;
    public $base_uri = 'https://example.com/';

    /**
     * @param File $file to be verified
     * @return bool|null which indicates whether the file was verified successfully or was not verified yet
     */
    public function verify(File $file)
    {

        $info = $this->cache->get("file_" . $file->id);

        if (!isset($info['is_verified'])) {
            if (!empty($info['queue_id'])) {

                $result = $this->getStatus($info['queue_id']);

                $info['attempts'] = empty($info['attempts']) ?: 1;

                switch ($result['status']) {
                    case "pending":

                        break;
                    case "verified":
                        $info['is_verified'] = true;
                        break;
                    case "error":
                        $info['is_verified']        = false;
                        $info['verification_error'] = $result['message'];
                        break;
                }

                $info['attempts']++;

            } else {
                $result           = $this->submit($file);
                $info['queue_id'] = $result['id'];
            }
        }

        $this->cache->save("file_" . $file->id, $info, 120);

        return !empty($info['is_verified']) ? $info['is_verified'] : null;

    }

    /**
     * @param File $file submits file to verification end-point
     * @return string returns the body of the response with queue ID
     * @throws
     */
    public function submit(File $file)
    {
        $response = $this->client->post('/api/verify', [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->getToken()
            ],
            'json'    => [
                'file' => $file->base64_string
            ]
        ]);

        return $response->getBody();
    }

    /**
     * @param $queue_id string or integer which indicates file's ID in verification queue
     * @return string returns the body of the response with verification status
     * @throws
     */
    public function getStatus($queue_id)
    {
        $response = $this->client->post('/api/status', [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->getToken()
            ],
            'json'    => [
                'id' => $queue_id
            ]
        ]);

        return $response->getBody();
    }

    /**
     * @return string with authentication token
     * @throws ExtractorException
     */
    public function getToken()
    {
        $auth = $this->cache->get("ServiceVerifierAuth");
        try {
            if (!$auth) {
                $response = $this->client->post('/gettoken', [
                    'auth' => [$this->email, $this->password]
                ]);

                $auth = json_decode($response->getBody(), true);

                $this->cache->save("ServiceVerifierAuth", $auth, $auth['expires_in']);

            }
        } catch (\Exception $e) {
            throw new ExtractorException($e->getMessage());
        }

        return $auth['access_token'];
    }

}