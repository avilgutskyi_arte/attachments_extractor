<?php


namespace Extractor\Verifier;


use Extractor\Entities\File;

interface VerifierInterface
{
    public function verify(File $file);
}
