<?php


namespace Extractor;


trait AttributesSetterTrait
{
    /**
     * @param $attributes array with attributes to be set in a specific class
     */
    public function setAttributes($attributes)
    {
        foreach ($attributes as $attribute => $value) {
            $this->$attribute = $value;
        }
    }
}