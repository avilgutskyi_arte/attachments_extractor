<?php


namespace Extractor\Entities;


class File
{
    public $id;
    public $name;
    public $size;
    public $file_path;
    public $base64_string;

    public function __construct()
    {
    }

    /**
     * Used to convert file in base64 string
     */
    public function toBase64String()
    {
        $this->base64_string = base64_encode(file_get_contents($this->file_path));
    }

    /**
     * Used to get file size
     */
    public function setSize()
    {
        $this->size = filesize($this->file_path);
    }

}