<?php


namespace Extractor;


use Extractor\Entities\File;

class Extractor
{
    private $downloader;
    private $verifier;
    private $uploader;

    /**
     * Extractor constructor.
     *
     * @param $config array with configurations for using needed classes for specific Extractor
     * @throws ExtractorException
     */
    public function __construct($config)
    {
        foreach ($config as $type => $data) {
            $class = '\Extractor\\' . ucfirst(strtolower($type)) . '\\' . ucfirst(strtolower($config[$type]['class'])) . ucfirst(strtolower($type));
            if (!class_exists($class)) {
                throw new ExtractorException("Class $class does not exist");
            } else {
                $this->$type = new $class($data['params']);
            }
        }
    }

    /**
     * Is used to perform the chain of download -> verify -> upload operation
     *
     * @throws ExtractorException
     */
    public function execute()
    {
        try {
            $files = $this->downloader->download();
            if (!count($files)) return;

            foreach ($files as $id => $file) {
                if ($this->filter($file, ["extension" => ["allowed" => ["pdf", "csv", "jpg", "jpeg", "png", "bmp", "gif", "svg"]]])) {
                    $file->toBase64String();
                } else {
                    unset($files[$id]);
                }
            }

            while (count($files)) {
                foreach ($files as $id => $file) {
                    $verification_result = $this->verifier->verify($file);
                    switch (1) {
                        case $verification_result === true:
                            $uploading_result = $this->uploader->upload($file);
                            if ($uploading_result === true) {
                                unset($files[$id]);
                            }
                            break;
                        case $verification_result === false:
                            unset($files[$id]);
                            break;
                        case $verification_result === null:
                            // to be checked once again
                            break;
                    }
                }
                sleep(3);
            }
        } catch (\Exception $e) {
            throw new ExtractorException($e->getMessage());
        }
    }

    /**
     * @param $file  File to be uploaded
     * @param $rules array to be applied for filtering ["classname" => $configuration]
     * @return bool
     */
    private function filter($file, $rules)
    {
        foreach ($rules as $filter => $attributes) {
            $class  = '\Extractor\\Filter\\' . ucfirst(strtolower($filter)) . "Filter";
            $filter = new $class();
            $filter->setAttributes($attributes);
            if (!$filter->filtrate($file)) {
                return false;
            }
        }

        return true;
    }
}