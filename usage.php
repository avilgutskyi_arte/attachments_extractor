<?php

require "vendor/autoload.php";

use Extractor\ExtractorException;
use Extractor\Worker;

try {
    $worker = new Worker(file_get_contents("data.json"));
    $worker->start();
} catch (ExtractorException $e) {
}
