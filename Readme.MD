This tool will help you download attachments from your emails, verify them, and upload them wherever you want.

Please use the following steps to start using it:
- Please copy `data.json.example` in `data.json` and fill in your credentials
- Please see classes in Downloader, Verifier and Uploader folder to extend this tool 